﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CitizenCardReader
{
    public class IdentityCard
    {
        public string birthDate { get; set; }
        public string deliveryEntity { get; set; }
        public string cardNumber { get; set; }
        public string cardNumberPAN { get; set; }
        public string cardVersion { get; set; }
        public string country { get; set; }
        public string documentType { get; set; }
        public string firstname { get; set; }
        public string firstnameFather { get; set; }
        public string firstnameMother { get; set; }
        public string height { get; set; }
        public string locale { get; set; }
        public string mrz1 { get; set; }
        public string mrz2 { get; set; }
        public string mrz3 { get; set; }
        public string name { get; set; }
        public string nameFather { get; set; }
        public string nameMother { get; set; }
        public string nationality { get; set; }
        public string notes { get; set; }
        public string numBI { get; set; }
        public string numNIF { get; set; }
        public string numSNS { get; set; }
        public string sex { get; set; }
        public string validityDate { get; set; }
        public string numSS { get; set; }
        public string deliveryDate { get; set; }
        public string addrType { get; set; }
        public string street { get; set; }
        public string municipality { get; set; }
        public string addressF { get; set; }
        public string building { get; set; }
        public string buildingAbbr { get; set; }
        public string countryM { get; set; }
        public string countryDescF { get; set; }
        public string cp3 { get; set; }
        public string cp4 { get; set; }
        public string district { get; set; }
        public string districtDesc { get; set; }
        public string door { get; set; }
        public string floor { get; set; }
        public string freguesia { get; set; }
        public string freguesiaDesc { get; set; }
        public string locality { get; set; }
        public string localityF { get; set; }
        public string municipalityDesc { get; set; }
        public string numMor { get; set; }
        public string numMorF { get; set; }
        public string postal { get; set; }
        public string postalF { get; set; }
        public string place { get; set; }
        public string regioF { get; set; }
        public string side { get; set; }
        public string streettype { get; set; }
        public string streettypeAbbr { get; set; }
        public string cityF { get; set; }
        public int authPinTriesLeft { get; set; }
        public int sigPinTriesLeft { get; set; }
        public int addrPinTriesLeft { get; set; }
        public string addr { get; set; }

    }
}
