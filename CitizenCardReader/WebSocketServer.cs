﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace CitizenCardReader
{
    class WebSocketServer : ICitizenCardReaderHandler
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private WebSocket webSocket = null;
        private ICitizenCardReaderHandler handler;
        private HttpListener listener = null;
        private bool stopped = false;

        public WebSocketServer()
        {
            handler = new Handler(this);
        }

        public void Start()
        {
            stopped = false;
            //RunEchoServer().Wait();
            RunEchoServer();
        }

        public void Stop()
        {
            stopped = true;
            listener.Close();
        }

        private async Task RunEchoServer()
        {
            listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();
            log.Debug("Web Socket Server started");
            Console.WriteLine("Started");


            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                if (!context.Request.IsWebSocketRequest)
                {
                    context.Response.Close();
                    continue;
                }

                log.Debug("New connection accepted");

                var wsContext = await context.AcceptWebSocketAsync(null);
                webSocket = wsContext.WebSocket;

                byte[] buffer = new byte[1024];
                WebSocketReceiveResult received = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                while (received.MessageType != WebSocketMessageType.Close)
                {

                    log.Debug($"Echoing {received.Count} bytes received in a {received.MessageType} message; Fin={received.EndOfMessage}");
                    Console.WriteLine($"Echoing {received.Count} bytes received in a {received.MessageType} message; Fin={received.EndOfMessage}");
                    // Echo anything we receive
                    //await webSocket.SendAsync(new ArraySegment<byte>(buffer, 0, received.Count), received.MessageType, received.EndOfMessage, CancellationToken.None);
                    Thread.Sleep(1000);
                    received = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                    //if (stopped)
                    //{
                    //    return;
                    //}
                }

                await webSocket.CloseAsync(received.CloseStatus.Value, received.CloseStatusDescription, CancellationToken.None);

                webSocket.Dispose();
                log.Debug("Connection finished");
                Console.WriteLine("Finished");

                /*if(stopped)
                {
                    return;
                }*/
            }
        }

        public void ReceiveCitizenCardInfo(String xml)
        {
        }

        public ICitizenCardReaderHandler getHandler()
        {
            return this.handler;
        }

        public WebSocket getSocket()
        {
            return this.webSocket;
        }
    }
}
