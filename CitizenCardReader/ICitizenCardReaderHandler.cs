﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CitizenCardReader
{
    interface ICitizenCardReaderHandler
    {
        void ReceiveCitizenCardInfo(String xml);
    }
}
