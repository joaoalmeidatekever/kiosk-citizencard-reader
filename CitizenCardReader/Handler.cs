﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;

namespace CitizenCardReader
{
    class Handler : ICitizenCardReaderHandler
    {
        private WebSocketServer socket;

        private static String ISSUE_DATE_STR = "DataEmissao";
        private static String VALID_UNTIL_DATE_STR = "DataValidade";
        private static String SURNAME_STR = "Apelido";
        private static String NAME_STR = "Nome";
        private static String GENDER_STR = "Sexo";
        private static String NATIONALITY_STR = "Nacionalidade";
        private static String BIRTH_DATE_STR = "DataNascimento";
        private static String HEIGHT_DATE_STR = "Altura";
        private static String ID_STR = "BI";
        private static String VAT_STR = "NumIdentFiscal";
        private static String SS_STR = "NumSegSocial";
        private static String HEALTH_STR = "NumUtenteSaude";

        public Handler(WebSocketServer socket)
        {
            this.socket = socket;
        }

        public void mPay_ReadCitizenCardRes(int nResult, string bstrCitizenXml, string bstrError)
        {
            //($"ReadCitizenCard Response: {nResult} ({bstrError})");
            //LogInfo($"CitizenCard  = \n{bstrCitizenXml}");
            //LogInfo("ReadCitizenCard Response handler exception: " + ex.Message);

        }

        void ICitizenCardReaderHandler.ReceiveCitizenCardInfo(string xml)
        {
            if(socket.getSocket() != null) { 
                Console.WriteLine("HANDLER: " + xml);
                XmlDocument xmlDoc = new XmlDocument(); // Create an XML document object
                xmlDoc.LoadXml(xml);

                // Get elements
                XmlNodeList cc = xmlDoc.GetElementsByTagName("Cidadao");

                IdentityCard id = new IdentityCard();
                id.deliveryDate = xmlDoc.FirstChild.Attributes[ISSUE_DATE_STR].Value;
                id.validityDate = xmlDoc.FirstChild.Attributes[VALID_UNTIL_DATE_STR].Value;
                id.name = xmlDoc.FirstChild.Attributes[SURNAME_STR].Value;
                id.firstname = xmlDoc.FirstChild.Attributes[NAME_STR].Value;
                id.sex = xmlDoc.FirstChild.Attributes[GENDER_STR].Value;
                id.nationality = xmlDoc.FirstChild.Attributes[NATIONALITY_STR].Value;
                id.birthDate = xmlDoc.FirstChild.Attributes[BIRTH_DATE_STR].Value;
                id.height = xmlDoc.FirstChild.Attributes[HEIGHT_DATE_STR].Value;
                id.numBI = xmlDoc.FirstChild.Attributes[ID_STR].Value;
                id.numNIF = xmlDoc.FirstChild.Attributes[VAT_STR].Value;
                id.numSS = xmlDoc.FirstChild.Attributes[SS_STR].Value;
                id.numSNS = xmlDoc.FirstChild.Attributes[HEALTH_STR].Value;

                Console.WriteLine("CC: " + JsonConvert.SerializeObject(id));


                socket.getSocket().SendAsync(new ArraySegment<byte>(System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(id))), WebSocketMessageType.Text, true, CancellationToken.None);
            } else
            {
                Console.WriteLine("socket--->" + socket.getSocket());
            }
        }
    }
}
