
@echo off
setlocal enabledelayedexpansion

Set Operation=Stoping the service .................................... 
<nul (set/p z=-^> %Operation%) 
@echo %date% %time% %Operation% > "%~dp0uninstall.log"

@echo %date% %time% Stopping "Newvision Card Payment" >> "%~dp0uninstall.log"
(net stop "Newvision Card Payment") 2>> "%~dp0uninstall.log" >nul

echo:Ok^^!
@echo %date% %time% %Operation% OK^^! >> "%~dp0uninstall.log"


set Operation=Uninstalling NewCardPayment Core ....................... 
<nul (set/p z=-^> %Operation%) 
@echo %date% %time% %Operation% >> "%~dp0uninstall.log"

@echo %date% %time% Unregistering service "%~dp0newcardpayment.exe" >> "%~dp0uninstall.log"
("%~dp0newcardpayment.exe" -UnregServer) 2>> "%~dp0uninstall.log" 
IF !errorlevel! NEQ 0 goto abort

@echo %date% %time% Unregistering "%~dp0newcardpaymentps.dll" >> "%~dp0uninstall.log"
regsvr32 /s /U "%~dp0newcardpaymentps.dll" 

echo:Ok^^!
@echo %date% %time% %Operation% OK^^! >> "%~dp0uninstall.log"


echo.
echo The uninstallation was successful!
echo.
%~d0
cd %~dp0
del "%~dp0uninstall.log"
if "%allusersprofile%" NEQ "C:\ProgramData" goto winxp
timeout /T 10
goto end

:abort
echo:ERROR^^!
echo.
echo:An ERROR occured^^!
echo.
echo:(see "%~dp0uninstall.log" for more details)
echo.
@echo %Operation% ERROR^^! >> "%~dp0uninstall.log"
%~d0
cd %~dp0

:winxp
pause

:end


