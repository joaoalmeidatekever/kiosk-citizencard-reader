@echo off
setlocal enabledelayedexpansion
set Operation=Installing VB Runtime ..................................... 
<nul (set/p z=-^> %Operation%) 
@echo %date% %time% %Operation% >> "%~dp0install.log"

SET InitialPath=%~dp0
@echo %date% %time% Installing "%InitialPath%Install\Msvbvm50.exe" >> "%~dp0install.log"
"%InitialPath%Install\Msvbvm50.exe" /q

IF !errorlevel! NEQ 0 goto vbwarning
goto vbok
:vbwarning
echo:Warning^^!
@echo %date% %time% %Operation% Failed^^! >> "%~dp0install.log"
goto vbskip
:vbok
echo:Ok^^!
@echo %date% %time% %Operation% OK^^! >> "%~dp0install.log"
:vbskip

set Operation=Installing VC redistributable packages .................... 
<nul (set/p z=-^> %Operation%) 
@echo %date% %time% %Operation% >> "%~dp0install.log"

SET InitialPath=%~dp0
@echo %date% %time% Installing "%InitialPath%Install\vcredist_x86.exe" >> "%~dp0install.log"
"%InitialPath%Install\vcredist_x86.exe" /q

IF !errorlevel! NEQ 0 goto vcwarning
goto vcok
:vcwarning
echo:Warning^^!
@echo %date% %time% %Operation% Failed^^! >> "%~dp0install.log"
goto vcskip
:vcok
echo:Ok^^!
@echo %date% %time% %Operation% OK^^! >> "%~dp0install.log"
:vcskip


set Operation=Installing MSXML4 ......................................... 
<nul (set/p z=-^> %Operation%) 
@echo %date% %time% %Operation% >> "%~dp0install.log"

md "%Temp%\NewCardPayment" 2>> "%~dp0install.log" 

@echo %date% %time% Extracting "%~dp0Install\MSXML4.EXE" >> "%~dp0install.log"
("%~dp0Install\MSXML4.EXE" /Q /T:"%TEMP%\NewCardPayment" /C) 2>> "%~dp0install.log" 
IF !errorlevel! NEQ 0 goto abort

@echo %date% %time% Running "%TEMP%\NewCardPayment\MSXML.MSI" >> "%~dp0install.log"
("%TEMP%\NewCardPayment\MSXML.MSI" /qn /norestart) 2>> "%~dp0install.log" 
IF !errorlevel! NEQ 0 goto abort

del "%TEMP%\NewCardPayment"\*.* /Q

echo:Ok^^!
@echo %date% %time% %Operation% OK^^! >> "%~dp0install.log"


set Operation=Installing NewCardPayment Core ............................ 
<nul (set/p z=-^> %Operation%) 
@echo %date% %time% %Operation% >> "%~dp0install.log"

(net stop "Newvision Card Payment") 2>> "%~dp0install.log" >nul

@echo %date% %time% Registering service "%~dp0NewCardPayment.exe" >> "%~dp0install.log"
("%~dp0NewCardPayment.exe" -service) 2>> "%~dp0install.log" 
IF !errorlevel! NEQ 0 goto abort

@echo %date% %time% Registering "%~dp0NewCardPaymentps.dll" >> "%~dp0install.log"
regsvr32 /s "%~dp0NewCardPaymentps.dll" 
IF !errorlevel! NEQ 0 goto abort

echo:Ok^^!
@echo %date% %time% %Operation% OK^^! >> "%~dp0install.log"

echo.
echo The installation was successful!
echo.
%~d0
cd %~dp0
del "%~dp0install.log"

if "%allusersprofile%" NEQ "C:\ProgramData" goto winxp
timeout /T 10
goto end

:abort
echo:ERROR^^!
echo.
echo:An ERROR occured^^!
echo.
echo:(see "%~dp0install.log" for more details)
echo.
@echo %Operation% ERROR^^! >> "%~dp0install.log"
%~d0
cd %~dp0

:winxp
pause

:end


