﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NEWCARDPAYMENTLib;

namespace CitizenCardReader
{
    class CitizenCardReaderWrap
    {
        private static PayCard mPay;

        private static int nPayment;
        private static bool bContinuousTest;

        private ICitizenCardReaderHandler handler;
        private Thread readerThread;

        private Boolean readerAvailable = true;
        private String currentCard = null;

        private int errorCount = 0;
        private static int MAX_ERROR_COUNT = 2;

        private int waitingCount = 0;
        private static int MAX_WAIT_COUNT = 20;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Thread wait4Enable;

        public CitizenCardReaderWrap(ICitizenCardReaderHandler handler)
        {
            this.handler = handler;
            Initialize();
        }

        public void Initialize()
        {

            //mPay = (PayCard)Activator.CreateInstance(Type.GetTypeFromCLSID(new Guid("6BD1A5C8-428E-496C-9B48-0F621CD1D4B3")));

            try
            {
                
                mPay = new PayCard();
                Thread.Sleep(100);
                mPay.StartModule();
                //mPay.SetTerminalParam(1, "V7");

                //mPay = (PayCard)Activator.CreateInstance(Type.GetTypeFromCLSID(new Guid("6BD1A5C8-428E-496C-9B48-0F621CD1D4B3")));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "EnableReaderRes").AddEventHandler(mPay, new _IPayCardEvents_EnableReaderResEventHandler(mPay_EnableReaderRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "StartPaymentRes").AddEventHandler(mPay, new _IPayCardEvents_StartPaymentResEventHandler(mPay_StartPaymentRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "CancelPaymentRes").AddEventHandler(mPay, new _IPayCardEvents_CancelPaymentResEventHandler(mPay_CancelPaymentRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "ServicePaymentRes").AddEventHandler(mPay, new _IPayCardEvents_ServicePaymentResEventHandler(mPay_ServicePaymentRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "GotoIdleRes").AddEventHandler(mPay, new _IPayCardEvents_GotoIdleResEventHandler(mPay_GotoIdleRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "IsCardPresentRes").AddEventHandler(mPay, new _IPayCardEvents_IsCardPresentResEventHandler(mPay_IsCardPresentRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "ReadKeyboardRes").AddEventHandler(mPay, new _IPayCardEvents_ReadKeyboardResEventHandler(mPay_ReadKeyboardRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "GetTracksRes").AddEventHandler(mPay, new _IPayCardEvents_GetTracksResEventHandler(mPay_GetTracksRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "RunChipCommandsRes").AddEventHandler(mPay, new _IPayCardEvents_RunChipCommandsResEventHandler(this.mPay_RunChipCommandsRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "GetChipBinRes").AddEventHandler(mPay, new _IPayCardEvents_GetChipBinResEventHandler(this.mPay_GetChipBinRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "ReadCitizenCardRes").AddEventHandler(mPay, new _IPayCardEvents_ReadCitizenCardResEventHandler(mPay_ReadCitizenCardRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "AuthorizationRes").AddEventHandler(mPay, new _IPayCardEvents_AuthorizationResEventHandler(mPay_AuthorizationRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "StartMaintenanceRes").AddEventHandler(mPay, new _IPayCardEvents_StartMaintenanceResEventHandler(mPay_StartMaintenanceRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "StartSupervisorRes").AddEventHandler(mPay, new _IPayCardEvents_StartSupervisorResEventHandler(mPay_StartSupervisorRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "OpenPeriodRes").AddEventHandler(mPay, new _IPayCardEvents_OpenPeriodResEventHandler(mPay_OpenPeriodRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "ClosePeriodRes").AddEventHandler(mPay, new _IPayCardEvents_ClosePeriodResEventHandler(mPay_ClosePeriodRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "CheckPeriodRes").AddEventHandler(mPay, new _IPayCardEvents_CheckPeriodResEventHandler(mPay_CheckPeriodRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "GetModuleVersionRes").AddEventHandler(mPay, new _IPayCardEvents_GetModuleVersionResEventHandler(mPay_GetModuleVersionRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "GetLastTransactionsRes").AddEventHandler(mPay, new _IPayCardEvents_GetLastTransactionsResEventHandler(mPay_GetLastTransactionsRes));
                new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "SetTerminalParamRes").AddEventHandler(mPay, new _IPayCardEvents_SetTerminalParamResEventHandler(mPay_SetTerminalParamRes));
                //new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "PurchaseAfterAuthorRes").AddEventHandler(mPay, new _IPayCardEvents_PurchaseAfterAuthorResEventHandler(mPay_PurchaseAfterAuthorRes));
                //new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "CloseOpenPeriodRes").AddEventHandler(mPay, new _IPayCardEvents_CloseOpenPeriodResEventHandler(mPay_CloseOpenPeriodRes));
                //new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "PrintDataRes").AddEventHandler(mPay, new _IPayCardEvents_PrintDataResEventHandler(mPay_PrintDataRes));
                //new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "CommunicationTestRes").AddEventHandler(mPay, new _IPayCardEvents_CommunicationTestResEventHandler(mPay_CommunicationTestRes));
                //new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "DeleteDataRes").AddEventHandler(mPay, new _IPayCardEvents_DeleteDataResEventHandler(mPay_DeleteDataRes));
                //new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "SoftwareUpdateRes").AddEventHandler(mPay, new _IPayCardEvents_SoftwareUpdateResEventHandler(mPay_SoftwareUpdateRes));
                //new ComAwareEventInfo(typeof(_IPayCardEvents_Event), "ConfirmPaymentRes").AddEventHandler(mPay, new _IPayCardEvents_ConfirmPaymentResEventHandler(mPay_ConfirmPaymentRes));
                log.Debug("Initialized!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //mPay.CloseModule();
                Console.ReadLine();
                mPay = null;
            }
        }

        public void Start()
        {
            readerThread = new Thread(new ThreadStart(Read));
            readerThread.Start();
            //readerThread.Join();
        }

        public void Stop()
        {
            readerThread.Interrupt();

            //mPay.CloseModule();
            mPay = null;
        }

        static void Main(string[] args)
        {
            try
            {
                //while (true)
                //{
                consoleHandler = new ConsoleEventDelegate(ConsoleEventCallback);
                SetConsoleCtrlHandler(consoleHandler, true);

                WebSocketServer socket = new WebSocketServer();

                Thread socketThread = new Thread(new ThreadStart(socket.Start));
                socketThread.Start();

                ICitizenCardReaderHandler hdl = socket.getHandler();

                Console.WriteLine("-->" + hdl);

                CitizenCardReaderWrap card = new CitizenCardReaderWrap(hdl);
                Thread cardThread = new Thread(new ThreadStart(card.Start));
                cardThread.Start();

                socketThread.Join();
                cardThread.Join();
                //card.Start();

                //Thread.Sleep(30000);
                //Console.WriteLine("Restart!");

                //socket.Stop();
                //socketThread.Interrupt();
                //card.Stop();
                //cardThread.Interrupt();
                //}
            }
            catch (Exception e)
            {
                try
                {
                    //mPay.CloseModule();
                }
                catch (Exception ex)
                {
                    //Empty

                    Console.WriteLine("EXCEPTION:" + ex.InnerException);
                }
                mPay = null;
                Console.WriteLine(e);
            }
        }

        private void stopWaitingEnable()
        {
            Thread.Sleep(17000);
            if(!readerAvailable)
            {
                mPay.GotoIdle(false, null, null);
                readerAvailable = true;
                currentCard = null;
            }
        }

        private void Read()
        {
            int errorNum = 0;
            int recover = 0;
            int sleepInterval = 5000;

            //Thread wait4Enable = new Thread(new ThreadStart(this.Start));
            //Thread cardThread = new Thread(new ThreadStart(card.Start));
            //this.wait4Enable = new Thread(new ThreadStart(this.stopWaitingEnable));

            while (true)
            {
                try
                {
                    if (readerAvailable)
                    {
                        recover = 0;
                        Thread.Sleep(2000);
                        errorNum = mPay.EnableReader(null, null);

                        //wait4Enable.Start();

                        log.Debug("reading...");
                        if (errorNum != 0)
                        {
                            log.Debug("Unable to enable reader");
                            readerAvailable = true;
                            currentCard = null;

                        } else
                        {
                            readerAvailable = false;
                            currentCard = null;
                        }
                    }
                    else
                    {
                        log.Debug("waiting");
                    }

                    Thread.Sleep(sleepInterval);
                }
                catch (Exception e)
                {
                    log.Debug("closing..." + e.Message);
                    try
                    {
                        Thread.Sleep(100);
                        //mPay.CloseModule();
                        Thread.Sleep(100);
                    }
                    catch (Exception ex)
                    {
                        //Empty
                        Console.WriteLine("EXCEPTION:" + ex.InnerException);
                    }
                    mPay = null;
                    Initialize();
                    readerAvailable = true;
                    waitingCount = 0;
                    currentCard = null;
                }
            }
        }

        public void mPay_EnableReaderRes(int nResult, int nCardType, ref object pvarCardInfo, string bstrError)
        {
            try
            {
                byte[] array = pvarCardInfo as byte[];
                string text = "";
                for (int i = 0; i < array.GetLength(0); i++)
                {
                    text = text + " " + array[i].ToString();
                }
                Console.WriteLine($"EnableReader Response: {nResult} CardType = {nCardType} ({bstrError}) ");
                Console.WriteLine("EnableReader Response: CardInfo = " + text);


                if (nResult != 0)
                {
                    //readerAvailable = true;
                    //currentCard = null;
                    mPay.GotoIdle(false, null, null);
                    Thread.Sleep(1000);
                } else {
                    Thread.Sleep(100);
                    mPay.ReadCitizenCard(false);
                }

                //currentCard = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("EnableReader Response handler exception: " + ex.Message);

                readerAvailable = true;
                waitingCount = 0;
                currentCard = null;
            }
        }

        private void mPay_StartPaymentRes(int nResult, string bstrDetailXml, string bstrReceipt, string bstrError)
        {
            int num = 0;
            //CancelPayment cancelPayment = null;
            try
            {
                Console.WriteLine($"StartPayment Response: {nResult} ({bstrError})");
                Console.WriteLine("Details = \n" + bstrDetailXml);
                //LogPrinter(bstrReceipt);
                if (nPayment > 0 && (nResult == 0 || nResult == 17))
                {
                    Console.WriteLine("Ending payment...");
                    if (nPayment == 2)
                    {
                        //cancelPayment = new CancelPayment();
                        /*if (cancelPayment.ShowDialog() == DialogResult.OK)
                        {
                            num = mPay.CancelPayment(cancelPayment.nPeriod, cancelPayment.nTransaction, "270", "OPERAÇÃO CANCELADA", "CANCELED OPERATION");
                        }
                        else
                        {
                            nPayment = 1;
                        }*/
                        if (num < 0)
                        {
                            Console.WriteLine($"Cancel payment failed: {num}");
                        }
                        Console.WriteLine("Ok");
                        if (nPayment == 1)
                        {
                            Console.WriteLine("A imprimir recibo");
                            nPayment = 0;
                        }
                        else
                        {
                            Console.WriteLine("A cancelar operação, por favor aguarde");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("StartPayment Response handler exception: " + ex.Message);
            }
            finally
            {
                //cancelPayment?.Dispose();
            }
        }

        private void mPay_ConfirmPaymentRes(int nResult, string bstrDetailXml, string bstrReceipt, string bstrError)
        {
            try
            {
                Console.WriteLine($"ConfirmPayment Response: {nResult} ({bstrError})");
                Console.WriteLine("Details = \n" + bstrDetailXml);
                //LogPrinter(bstrReceipt);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ConfirmPayment Response handler exception: " + ex.Message);
            }
        }

        private void mPay_CancelPaymentRes(int nResult, string bstrError)
        {
            try
            {
                Console.WriteLine($"CancelPayment Response: {nResult} ({bstrError})");
                if (nPayment > 0)
                {
                    switch (nResult)
                    {
                        case 0:
                            if (nPayment == 1)
                            {
                                Console.WriteLine("Por favor aguarde o recibo");
                            }
                            else
                            {
                                Console.WriteLine("Operação cancelada com sucesso");
                            }
                            break;
                        case 17:
                            if (nPayment == 1)
                            {
                                Console.WriteLine("Não vai ser possível imprimir o seu recibo");
                            }
                            else
                            {
                                Console.WriteLine("Operação cancelada com sucesso");
                            }
                            break;
                    }
                    nPayment = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("StartPayment Response handler exception: " + ex.Message);
            }
        }

        private void mPay_ServicePaymentRes(int nResult, string bstrDetailXml, string bstrReceipt, string bstrError)
        {
            try
            {
                Console.WriteLine($"ServicePayment Response: {nResult} ({bstrError})");
                Console.WriteLine("Details = \n" + bstrDetailXml);
                //LogPrinter(bstrReceipt);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ServicePayment Response handler exception: " + ex.Message);
            }
        }

        private void mPay_GotoIdleRes(int nResult, string bstrError)
        {
            try
            {
                Console.WriteLine($"GotoIdle Response: {nResult} ({bstrError})");
                readerAvailable = true;
                currentCard = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("GotoIdle Response handler exception: " + ex.Message);


                readerAvailable = true;
                waitingCount = 0;
                currentCard = null;
            }
        }

        private void mPay_IsCardPresentRes(int nResult, int bPresent, string bstrError)
        {
            try
            {
                Console.WriteLine($"IsCardPresent Response: {nResult} ({bstrError})");
                Console.WriteLine($"Present = {bPresent}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("IsCardPresent Response handler exception: " + ex.Message);
            }
        }

        private void mPay_ReadKeyboardRes(int nResult, string bstrDigits, string bstrError)
        {
            try
            {
                Console.WriteLine($"ReadKeyboard Response: {nResult} ({bstrError})");
                Console.WriteLine($"Digits = {bstrDigits}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("ReadKeyboard Response handler exception: " + ex.Message);
            }
        }

        private void mPay_GetTracksRes(int nResult, string bstrTrack1, string bstrTrack2, string bstrError)
        {
            try
            {
                Console.WriteLine($"GetTracks Response: {nResult} ({bstrError})");
                Console.WriteLine($"Track1 = \n{bstrTrack1}");
                Console.WriteLine($"Track2 = \n{bstrTrack2}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTracks Response handler exception: " + ex.Message);
            }
        }

        private void mPay_RunChipCommandsRes(int nResult, ref object pvarSizes, ref object pvarResponses, string bstrError)
        {
            long num = 0L;
            try
            {
                Console.WriteLine($"RunChipCommands Response: {nResult} ({bstrError})");
                byte[] array = pvarResponses as byte[];
                long[] array2 = pvarSizes as long[];
                if (array == null || array2 == null)
                {
                    Console.WriteLine("RunChipCommands response array is null:");
                }
                else
                {
                    long[] array3 = array2;
                    int num2 = 0;
                    while (true)
                    {
                        if (num2 < array3.Length)
                        {
                            long num3 = array3[num2];
                            if (num3 + num <= array.GetLength(0))
                            {
                                string text = "";
                                for (int i = 0; i < num3; i++)
                                {
                                    text += string.Format("{0,2:X} ", array[i + num]);
                                }
                                Console.WriteLine(text);
                                num += num3;
                                num2++;
                                continue;
                            }
                            break;
                        }
                        return;
                    }
                    Console.WriteLine("RunChipCommands Response: data size is different from expected");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("RunChipCommands Response handler exception: " + ex.Message);
            }
        }

        private void mPay_GetChipBinRes(int nResult, ref object pvarCardBin, string bstrError)
        {
            string text = "";
            try
            {
                byte[] array = pvarCardBin as byte[];
                if (array != null)
                {
                    byte[] array2 = array;
                    foreach (byte b in array2)
                    {
                        text += $"{(char)b} ";
                    }
                }
                Console.WriteLine($"GetChiBin Response: {nResult} ({bstrError})");
                Console.WriteLine("GetChipBin Response: Bin = " + text);
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetChiBin Response handler exception: " + ex.Message);
            }
        }

        private void mPay_ReadCitizenCardRes(int nResult, string bstrCitizenXml, string bstrError)
        {
            try
            {
                Console.WriteLine($"ReadCitizenCard Response: {nResult} ({bstrError})");
                Console.WriteLine($"CitizenCard  = \n{bstrCitizenXml}");
                
                //currentCard = bstrCitizenXml;

                if(nResult != 0)
                {
                    //readerAvailable = true;
                    //currentCard = null;

                    mPay.GotoIdle(false, null, null);
                }
                else
                {
                    handler.ReceiveCitizenCardInfo(bstrCitizenXml);
                    mPay.GotoIdle(false, null, null);
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("ReadCitizenCard Response handler exception: " + ex.Message);
            }
        }

        private void mPay_AuthorizationRes(int nResult, string bstrDetailXml, string bstrError)
        {
            try
            {
                Console.WriteLine($"Authorization Response: {nResult} ({bstrError})");
                Console.WriteLine($"Authorization data  = \n{bstrDetailXml}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Authorization Response handler exception: " + ex.Message);
            }
        }

        private void mPay_StartMaintenanceRes(int nResult, string bstrError)
        {
            try
            {
                Console.WriteLine($"StartMaintenance Response: {nResult} ({bstrError})");
            }
            catch (Exception ex)
            {
                Console.WriteLine("StartMaintenance Response handler exception: " + ex.Message);
            }
        }

        private void mPay_StartSupervisorRes(int nResult, string bstrError)
        {
            try
            {
                Console.WriteLine($"StartSupervisor Response: {nResult} ({bstrError})");
            }
            catch (Exception ex)
            {
                Console.WriteLine("StartSupervisor Response handler exception: " + ex.Message);
            }
        }

        private void mPay_OpenPeriodRes(int nResult, string bstrTPA, byte byPeriod, int nPeriod, int nTrans, string bstrData, string bstrError)
        {
            try
            {
                Console.WriteLine($"OpenPeriod Response: {nResult} ({bstrError})");
                Console.WriteLine("TPA = " + bstrTPA);
                Console.WriteLine("PeriodState = " + byPeriod.ToString());
                Console.WriteLine("Period = " + nPeriod.ToString());
                Console.WriteLine("Transaction = " + nTrans.ToString());
                //LogPrinter("Data = \n" + bstrData);
            }
            catch (Exception ex)
            {
                Console.WriteLine("OpenPeriod Response handler exception: " + ex.Message);
            }
        }

        private void mPay_ClosePeriodRes(int nResult, string bstrTPA, byte byPeriod, int nPeriod, int nTrans, string bstrData, string bstrDetailXml, string bstrError)
        {
            try
            {
                Console.WriteLine($"ClosePeriod Response: {nResult} ({bstrError})");
                Console.WriteLine("TPA = " + bstrTPA);
                Console.WriteLine("PeriodState = " + byPeriod.ToString());
                Console.WriteLine("Period = " + nPeriod.ToString());
                Console.WriteLine("Transaction = " + nTrans.ToString());
                //LogPrinter("Data = \n" + bstrData);
                Console.WriteLine("Details = \n" + bstrDetailXml);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ClosePeriod Response handler exception: " + ex.Message);
            }
        }

        private void mPay_CloseOpenPeriodRes(int nResult, string bstrTPA, byte byPeriod, int nPeriod, int nTrans, string bstrData, string bstrDetailXml, string bstrError)
        {
            try
            {
                Console.WriteLine($"CloseOpenPeriod Response: {nResult} ({bstrError})");
                Console.WriteLine("TPA = " + bstrTPA);
                Console.WriteLine("PeriodState = " + byPeriod.ToString());
                Console.WriteLine("Period = " + nPeriod.ToString());
                Console.WriteLine("Transaction = " + nTrans.ToString());
                //LogPrinter("Data = \n" + bstrData);
                Console.WriteLine("Details = \n" + bstrDetailXml);
            }
            catch (Exception ex)
            {
                Console.WriteLine("CloseOpenPeriod Response handler exception: " + ex.Message);
            }
        }

        private void mPay_PrintDataRes(int nResult, string bstrData, string bstrError)
        {
            try
            {
                Console.WriteLine($"PrintData Response: {nResult} ({bstrError})");
                //LogPrinter("Data = \n" + bstrData);
            }
            catch (Exception ex)
            {
                Console.WriteLine("PrintData Response handler exception: " + ex.Message);
            }
        }

        private void mPay_SoftwareUpdateRes(int nResult, string bstrData, string bstrError)
        {
            try
            {
                Console.WriteLine($"SoftwareUpdate Response: {nResult} ({bstrError})");
                //LogPrinter("Data = \n" + bstrData);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SoftwareUpdate Response handler exception: " + ex.Message);
            }
        }

        private void mPay_DeleteDataRes(int nResult, string bstrError)
        {
            try
            {
                Console.WriteLine($"DeleteData Response: {nResult} ({bstrError})");
            }
            catch (Exception ex)
            {
                Console.WriteLine("DeleteData Response handler exception: " + ex.Message);
            }
        }

        private void mPay_CommunicationTestRes(int nResult, string bstrError)
        {
            try
            {
                Console.WriteLine($"CommunicationTest Response: {nResult} ({bstrError})");
            }
            catch (Exception ex)
            {
                Console.WriteLine("CommunicationTest Response handler exception: " + ex.Message);
            }
        }

        private void mPay_CheckPeriodRes(int nResult, int bOpen, string bstrTPA, int nPeriod, byte byPeriod, string bstrError)
        {
            try
            {
                Console.WriteLine($"CheckPeriod Response: {nResult} ({bstrError})");
                Console.WriteLine("Open = " + bOpen.ToString());
                Console.WriteLine("TPA = " + bstrTPA);
                Console.WriteLine("PeriodState = " + byPeriod.ToString());
                Console.WriteLine("Period = " + nPeriod.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("CheckPeriod Response handler exception: " + ex.Message);
            }
        }

        private void mPay_GetModuleVersionRes(int nResult, string bstrVersion, string bstrError)
        {
            try
            {
                Console.WriteLine($"GetModuleVersion Response: {nResult} ({bstrError})");
                Console.WriteLine("Version = \n" + bstrVersion);
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetModuleVersion Response handler exception: " + ex.Message);
            }
        }

        private void mPay_GetLastTransactionsRes(int nResult, string bstrTransactions, string bstrError)
        {
            try
            {
                Console.WriteLine($"GetLastTransactions Response: {nResult} ({bstrError})");
                Console.WriteLine("Transactions = \n" + bstrTransactions);
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetLastTransactions Response handler exception: " + ex.Message);
            }
        }

        private void mPay_SetTerminalParamRes(int nResult, string bstrError)
        {
            try
            {
                Console.WriteLine($"SetTerminalParam Response: {nResult} ({bstrError})");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SetTerminalParam Response handler exception: " + ex.Message);
            }
        }

        private void mPay_PurchaseAfterAuthorRes(int nResult, string bstrDetailXml, string bstrReceipt, string bstrError)
        {
            try
            {
                Console.WriteLine($"PurchaseAfterAuthor Response: {nResult} ({bstrError})");
                Console.WriteLine("Details = \n" + bstrDetailXml);
                //LogPrinter("Receipt = \n" + bstrReceipt);
            }
            catch (Exception ex)
            {
                Console.WriteLine("PurchaseAfterAuthor Response handler exception: " + ex.Message);
            }
        }

        static bool ConsoleEventCallback(int eventType)
        {
            if (eventType == 2)
            {
                Console.WriteLine("Console window closing, death imminent");
                mPay.CloseModule();
            }
            return false;
        }
        static ConsoleEventDelegate consoleHandler;   // Keeps it from getting garbage collected
                                               // Pinvoke
        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);
    }
}
